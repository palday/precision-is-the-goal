---
title: "Optional stopping, Bayes, estimation and evidence"
subtitle: "Riffing on John Kruschke and Data Colada"
author: Phillip M. Alday
date: 24 February 2020
location: M/EEG lunch meeting, DCCN
---

Download the latest version of the slides [here](https://gitlab.com/palday/precision-is-the-goal/-/jobs/artifacts/master/raw/presentation.pdf?job=build).


