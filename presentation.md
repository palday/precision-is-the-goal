---
title: "~~Precision~~ Prediction is the goal: Optional stopping, Bayes, estimation and evidence"
subtitle: "Riffing on John Kruschke and Data Colada"
author: Phillip M. Alday
date: 24. February 2020
bibliography: bibliography.bib
theme: Singapore
slide-level: 2
csl: apa.csl
lang: en-US
header-includes:
- \usepackage{csquotes}
- \usepackage{tikz}
- \definecolor{MPIorange}{HTML}{ED6B06}
- \definecolor{MPIcyan}{HTML}{00786A}
- \definecolor{MPIlcyan}{HTML}{80BBB4}
- \setbeamercolor{local structure}{fg=MPIorange}
- \setbeamercolor{structure}{fg=MPIcyan}
- \setbeamercolor{alerted text}{fg=MPIorange}
include-before:
- \tikzset{every picture/.append style={trim left=0}}
---


# Power simulations are hard

## Effect sizes in EEG are hard.

- How many µV difference do we expect?
- How much single-trial noise?
    - How much classical filtering?
    - How much temporal averaging?
    - How many artifacts?
- How much intra-subject variability?
- How much inter-subject variability?
- How much topographical spread do we expect? What's a good reference electrode?
- If doing classical baseline correction [and you shouldn't, see @alday2019p], how will the noise in the baseline interval contribute to the noise in the window of interest?
- ...

. . .

We need good answers to at least some of these questions before we can do any type of power simulation.

# What if we didn't have to simulate ahead of time?

## Back a step: Why do we need power a priori? {.allowframebreaks}

- **Power** is a measure of our ability to measure a true effect of *a certain size* (relative to noise).
    - So we can be way off by not knowing how big the raw effect is.
    - Or how much noise there is.
    - Standardized effect sizes try to combine these into a unitless measure, but these have their own issues, especially for more complex partitioning of variance.
- Estimating power post-hoc is difficult and problematic because it's contingent on observed signifance [@hoenig.heisey:2001as]and we know this to be very biased [@gelmancarlin2014a]. It is also touches upon issues with HARKing and just-so stories.
- Optional stopping with traditional methods also isn't an option due to multiple comparisons. See [\textcolor{MPIcyan}{this simulation}](https://palday.shinyapps.io/optional_stopping/) for an example.
- Bayes factors using naive/default methods are also misleading. (**discussion point**, see [\textcolor{MPIcyan}{a}](http://datacolada.org/78a) [\textcolor{MPIcyan}{b}](http://datacolada.org/78b) [\textcolor{MPIcyan}{c}](http://datacolada.org/78c) and [\textcolor{MPIcyan}{d}](https://djnavarro.net/post/a-personal-essay-on-bayes-factors/)).
- Estimation ["The New Statistics", cf. @cumming2014a] seems to fare better than testing, but even then we run into difficulties if we're using frequentist confidence intervals (more on this later, but check out the simulation).


## Living with uncertainty {.allowframebreaks}

- Point estimates are highly variable. That's why we do statistics and not "just" curve-fitting!
- Treating a theoretical parameter value as a point is also problematic. As Meehl pointed out [-@meehl:1967ps], it gets arbitrarily easy to reject trivial theoretical points with increasing data.

- In other words: data gives us more precision, but not always more accuracy.

- We need to allow for practical uncertainty around our theoretical points: a Region of Practical Equivalence [ROPE,@kruschke2018aimapips].

- This, combined with a Bayesian take on estimation [@kruschke.liddell:2017pbr], provides an integrative framework: "The Bayesian New Statistics".

- Note that you can always get hypothesis tests from estimates with uncertainty, but not vice-versa [see also @yarkoni.westfall:2017pops].

### Prediction is the goal.

- Setting an optional stopping rule based on an estimate with uncertainty, not contingent on a point null-hypothesis, allows us to stop when we have enough precision to discuss effects, instead of whether or not we found the effect we wanted.


## Using a ROPE to climb out of a difficult situation {.plain}
\centering \tiny from @kruschke2018aimapips

![](rope-rule.png)

## Why can't I use frequentist confidence intervals for this?  {.allowdisplaybreaks}

- Frequentist confidence intervals can be thought of as inversions of a given statistical test and hence p-value.

- So they run into some difficulties in terms of multiple testing, definitely on the philosophical level and potentially in practice.

- In particular, the challenge with CIs is that we can't make statements about the probability that a given CI contains the true value, only the long-run probability over all such constructed CIs

- (Bizarrely enough, "confidence" is a procedure in frequentist land.)

. . .

- This interacts with Meehl's paradox: our CIs will continue to shrink as we collect more data, but we have no indication that a given CI, which is "centered" around its point estimate, will contain the true value. So we can easily wind up excluding the true value *even with a ROPE*.


### More directly, this arises from the difference between likelihood- and posterior-based procedures. The latter is a variable estimate properly conditioned on the observed data, when proper priors are used.

## What do I need to use this method? {.allowdisplaybreaks .allowframebreaks}

Realistically, the same things you need to use Bayes properly:

### Proper priors
- Default, "uninformative" (including maximum entropy priors) priors are just as much a bad strawman as the classical null-hypothesis.
- Flat priors `{\tiny (over a non-finite interval)}`{=latex} are *improper* and reduce down to a frequentist model under maximum a-posteriori estimation, at least for the marginal estimates.
- I recommend regularizing / shrinkange priors that assume a small effect size, but are still fairly sensitive. For example, for an N400 experiment with a violation paradigm, I would assume most real effects are on the order of ±1µV, so my prior would be Normal(0,1), which is the mathematical formulation of "a majority of effects are on the order of 1µV, but some are up to 2µ, and effects larger than that are not common."


### A ROPE
- Like priors, a ROPE must be defined ahead of time.
- In this sense, a ROPE is a prior on what equivalence is.
- Good candidates for ROPEs are:
    - measurement precision
    - notions of "triviality"
- Note that a ROPE implies precision on the null hypothesis, but does not strongly imply precision of the HDI.

### An escape valve (optional, but recommended)
- Some specification of a maximal amount of data to collect before deciding to live with indeterminacy.
- Reaching this limit may indicate one of several things:
    - large interindividual differences that lead to indeterminacy at the population level
    - missing experimental control on some (potential) covariate
    - other sources of *inconsistency*

### A desired precision of the HDI (optional)
- The ROPE procedure places no hard constraint on the precision of the HDI.
    - For accepting the null, the precision of HDI must be smaller than the ROPE.
    - The precision of the HDI required for accepting an effect is dependent on the value of its mode.
    - Sufficiently large effects can be very imprecise.
- Providing an additional contraint on the HDI precision allows for stronger statements.


---

## Know when to stop

The combined stopping rule consists of a few steps:

1. If HDI completely within ROPE, then stop, else ...
2. If sample size > escape valve, then stop, else ...
3. If desired precision of HDI attained, then stop, else ...
4. Get more data.

...

PROFIT!

. . .

\alert{Priors, model structure (or possibly model structure selection procedure), ROPEs, escape valves and desired precision are great things to pre-register.}


## References {.allowframebreaks}


### Internets
\small

Kruschke, John, "Precision is the Goal" on Youtube: Part [\textcolor{MPIcyan}{1}](https://www.youtube.com/watch?v=lh5btlAvrLs) [\textcolor{MPIcyan}{2}](https://www.youtube.com/watch?v=ObCrb5I49qo&t=6s) [\textcolor{MPIcyan}{3}](https://www.youtube.com/watch?v=OIX6d2YEB04) [\textcolor{MPIcyan}{4}](https://www.youtube.com/watch?v=bKj5irH99OI)

Navarro, Danielle. "A personal essay on Bayes factors" [\textcolor{MPIcyan}{via Wayback Machine}](https://web.archive.org/save/https://djnavarro.net/post/a-personal-essay-on-bayes-factors/)

Simonsohn, Uri. Data Colada #78, a series on Bayes Factors: [\textcolor{MPIcyan}{a}](http://datacolada.org/78a) [\textcolor{MPIcyan}{b}](http://datacolada.org/78b) [\textcolor{MPIcyan}{c}](http://datacolada.org/78c)

### Traditional
\tiny
